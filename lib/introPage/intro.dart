import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter1/introPage/components/imageintro.dart';
import 'package:flutter1/introPage/components/titleintro.dart';
import 'package:flutter1/introPage/components/buttonintro.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({super.key});

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
          child: Container(
            decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
            child: const Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ImageIntro(),
                TitleIntro(),
                ButtonIntro(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
