import 'package:flutter/material.dart';

class ImageIntro extends StatelessWidget {
  const ImageIntro({super.key});

  @override
  Widget build(BuildContext context) {
    return Image.network(
      'https://cdn-icons-png.flaticon.com/512/4481/4481883.png',
      width: 200,
      fit: BoxFit.cover,
    );
  }
}
