import 'package:flutter/material.dart';
import 'package:flutter1/homePage/home.dart';

class ButtonIntro extends StatelessWidget {
  const ButtonIntro({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 270,
      height: 50,
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: Colors.red[900], // foreground
        ),
        onPressed: () => {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const HomePage()),
          ),
        },
        child: const Text(
          'Enter Shop',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
