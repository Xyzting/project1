// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter1/homePage/components/searchhome.dart';
import 'package:flutter1/homePage/components/titlehome.dart';
// import 'package:flutter1/homePage/component/buttonColumn.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
        child: const Column(
          children: [
            TitleHome(),
            SearchHome(),
          ],
        ),
      ),
    );
  }
}
