import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TitleHome extends StatelessWidget {
  const TitleHome({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 40,
                  height: 40,
                  decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 241, 240, 240),
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                  ),
                  alignment: Alignment.center,
                  padding: const EdgeInsets.only(bottom: 1),
                  child: const Icon(
                    FontAwesomeIcons.bars,
                    size: 17,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 40,
            height: 40,
            decoration: const BoxDecoration(
              color: Color.fromARGB(255, 241, 240, 240),
              borderRadius: BorderRadius.all(
                Radius.circular(50),
              ),
            ),
            alignment: Alignment.center,
            padding: const EdgeInsets.only(bottom: 1),
            child: const Icon(
              FontAwesomeIcons.bell,
              size: 17,
            ),
          ),
        ],
      ),
    );
  }
}
