import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SearchHome extends StatelessWidget {
  const SearchHome({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 450,
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 241, 240, 240),
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: const Icon(
              FontAwesomeIcons.magnifyingGlass,
              // color: Colors.white70,
              size: 17,
            ),
          ),
          const TextField(
            decoration: InputDecoration(
              border: InputBorder.none,
              labelText: 'search',
            ),
          ),
        ],
      ),
    );
  }
}
